from .housekeeper import clean_expired_auditlog


def test_expire_auditlog():
    res = [x for x in clean_expired_auditlog(year=2021, month=1, retention=365)]
    # batches should be 10-ish days
    assert len(res) == 4
    assert res[0] == (
        "DELETE FROM auditlog T1 WHERE T1.clock "
        "BETWEEN 1609459200 AND 1610323190 AND "
        "T1.clock < EXTRACT('epoch' FROM current_timestamp - INTERVAL '365 days');"
    )
